Hardware design for Arduino UNO-based PIC programmer
============================

This design is for making an arduino UNO shield according to the schematic
described in [Ardpicprog site](http://rweather.github.io/ardpicprog/index.html), 
which is an Arduino-based solution for programming
PIC microcontrollers from Microchip, such as PIC12F675,
PIC16F886 and others. 

See the [documentation](http://rweather.github.com/ardpicprog/)
for more information on the project.

![shield](ardpicprog-hardware.png)